<?php

namespace FirstIgnite\LaravelMeetingNeoEloquent\Tests;

use Illuminate\Database\Eloquent\Factories\Factory;
use FirstIgnite\LaravelMeetingNeoEloquent\MeetingServiceProvider;
use Orchestra\Testbench\TestCase as Orchestra;

class TestCase extends Orchestra
{
    public function setUp(): void
    {
        parent::setUp();

        Factory::guessFactoryNamesUsing(
            function (string $modelName) {
              return 'FirstIgnite\\LaravelMeetingNeoEloquent\\Database\\Factories\\'.class_basename($modelName).'Factory';
            }
        );
    }

    protected function getPackageProviders($app)
    {
        return [
            MeetingServiceProvider::class,
        ];
    }

    public function getEnvironmentSetUp($app)
    {
        $app['config']->set('database.default', 'sqlite');
        $app['config']->set('database.connections.sqlite', [
            'driver' => 'sqlite',
            'database' => ':memory:',
            'prefix' => '',
        ]);

        /*
        include_once __DIR__.'/../database/migrations/create_meetings_table.php.stub';
        (new \CreatePackageTable())->up();
        */
    }
}
