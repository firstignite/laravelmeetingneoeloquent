<?php

namespace FirstIgnite\LaravelMeetingNeoEloquent\Exceptions;

use FirstIgnite\LaravelMeetingNeoEloquent\MeetingAdder;
use FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting;

class NoZoomRoomAvailable extends \Exception
{

    /**
     * @var \FirstIgnite\LaravelMeetingNeoEloquent\MeetingAdder
     */
    protected MeetingAdder $meeting;

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\MeetingAdder $meeting
     * @return self
     */
    public static function create(MeetingAdder $meeting): self
    {
        return new static(
            'There is no Zoom Room available between `%s` and `%s` to host the meeting with topic `%s`',
            $meeting
        );
    }

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting $meeting
     * @return self
     */
    public static function createFromModel(Meeting $meeting): self
    {
        $meetingAdder = (new MeetingAdder)
            ->scheduledBy($meeting->scheduler)
            ->presentedBy($meeting->presenter)
            ->hostedBy($meeting->host)
            ->withTopic($meeting->topic)
            ->startingAt($meeting->start_time)
            ->during($meeting->duration);

        return static::create($meetingAdder);
    }

    /**
     * Create a new instance of NoZoomRoomAvailable exception
     *
     * @param string $message
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\MeetingAdder $meeting
     */
    public function __construct(string $message, MeetingAdder $meeting)
    {
        $this->meeting = $meeting;
        $this->message = sprintf(
            $message,
            $meeting->startTime->format('Y-m-d H:i:se'),
            (clone $meeting->startTime)->addMinutes($meeting->duration)->format('Y-m-d H:i:se'),
            $meeting->topic
        );
    }

    /**
     * Get the meeting that regeneted the exception
     *
     * @return \FirstIgnite\LaravelMeetingNeoEloquent\MeetingAdder
     */
    public function getMeeting(): MeetingAdder
    {
        return $this->meeting;
    }
}
