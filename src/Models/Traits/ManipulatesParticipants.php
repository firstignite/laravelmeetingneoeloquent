<?php

namespace FirstIgnite\LaravelMeetingNeoEloquent\Models\Traits;

use FirstIgnite\LaravelMeetingNeoEloquent\Contracts\Participant;
use FirstIgnite\LaravelMeetingNeoEloquent\Exceptions\BusyForTheMeeting;
use FirstIgnite\LaravelMeetingNeoEloquent\Models\Participant as ParticipantPivot;

/**
 * Provides verification methods for a meeting model
 */
trait ManipulatesParticipants
{
    /**
     * Check if the meeting has a given participant
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Contracts\Participant $participant
     * @return bool
     */
    public function hasParticipant(Participant $participant): bool
    {
        $morphType = get_class($participant);
        // return $this->participants()->where('id', $participant->id)->exists();
        $edge = $this->participants($morphType)->edge($participant);
        if ($edge) return true;
        return false;
        // $edge = $this->participants($morphType)->edge($participant);

        // return $this->participants($morphType)->where([
        //     'participant_id' => $participant->id,
        //     'participant_type' => $morphType,
        // ])->exists();
    }

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Contracts\Participant $participant
     * @return \FirstIgnite\LaravelMeetingNeoEloquent\Models\Participant|null
     */
    public function participant(Participant $participant) // remove type hint for now
    {
        $morphType = get_class($participant);

        // $participant = $this->participants($morphType)->where([
        //     'participant_id' => $participant->id,
        //     'participant_type' => $morphType,
        // ])->first();
        $participant = $this->participants($morphType)->where(
            'uuid',
            $participant->uuid,
        )->first();

        return $participant ? $participant : null;
    }

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Contracts\Participant $participant
     * @return \FirstIgnite\LaravelMeetingNeoEloquent\Models\Participant
     */
    public function addParticipant(Participant $participant): Participant
    {
        if ($this->hasParticipant($participant)) {
            throw \FirstIgnite\LaravelMeetingNeoEloquent\Exceptions\ParticipantAlreadyAdded::create($participant, $this);
        }

        // if (! config('meeting.allow_concurrent_meetings.participant')
        //     && $participant->isBusyBetween($this->start_time, $this->end_time)
        // ) {
        //     throw BusyForTheMeeting::createForParticipant($this, $participant);
        // }

        $this->instance->participantAdding($participant, $this, $uuid = \Illuminate\Support\Str::uuid()->toString());

        // $this->participants(get_class($participant))->save($participant, [
        //     'uuid' => $uuid,
        // ]);

        // $this->instance->participantAdded(
        //     $createdParticipant = $this->participant($participant)
        // );
        $this->instance->participantAdded($participant);

        return $participant;
    }

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Contracts\Participant $participant
     * @throws \FirstIgnite\LaravelMeetingNeoEloquent\Exceptions\ParticipantNotRegistered
     * @return bool
     */
    public function cancelParticipation(Participant $participant): bool
    {
        if (!$participantPivot = $this->participant($participant)) {
            throw \FirstIgnite\LaravelMeetingNeoEloquent\Exceptions\ParticipantNotRegistered::create($participant, $this);
        }

        $this->instance->participationCanceling($participantPivot);

        $canceled = $participantPivot->cancel();

        $this->instance->participationCanceled($participantPivot);

        return $canceled;
    }

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Contracts\Participant $participant
     * @throws \FirstIgnite\LaravelMeetingNeoEloquent\Exceptions\ParticipantNotRegistered
     * @return \Carbon\Carbon
     */
    public function joinParticipant(Participant $participant): ParticipantPivot
    {
        if (!$participantPivot = $this->participant($participant)) {
            throw \FirstIgnite\LaravelMeetingNeoEloquent\Exceptions\ParticipantNotRegistered::create($participant, $this);
        }

        $this->instance->participantJoining($participantPivot, $this);

        $this->instance->participantJoined(
            $participantPivot = $this->participant($participant)->join()
        );

        return $participantPivot;
    }

    /**
     * Undocumented function
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Contracts\Participant $participant
     * @throws \FirstIgnite\LaravelMeetingNeoEloquent\Exceptions\ParticipantNotRegistered
     * @return \Carbon\Carbon
     */
    public function leaveParticipant(Participant $participant): ParticipantPivot
    {
        if (!$participantPivot = $this->participant($participant)) {
            throw \FirstIgnite\LaravelMeetingNeoEloquent\Exceptions\ParticipantNotRegistered::create($participant, $this);
        }

        $this->instance->participantLeaving($participantPivot, $this);

        $this->instance->participantLeft(
            $participantPivot = $this->participant($participant)->leave()
        );

        return $participantPivot;
    }
}
