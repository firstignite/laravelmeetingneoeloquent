<?php

namespace FirstIgnite\LaravelMeetingNeoEloquent\Models\Traits;

// use Illuminate\Database\Eloquent\Relations\HasMany;
// use Illuminate\Database\Eloquent\Relations\HasOne;
// use Illuminate\Database\Eloquent\Relations\MorphTo;
use Vinelab\NeoEloquent\Eloquent\Relations\HasOne;
use Vinelab\NeoEloquent\Eloquent\Relations\HasMany;
use Vinelab\NeoEloquent\Eloquent\Relations\MorphTo;
use Vinelab\NeoEloquent\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use FirstIgnite\LaravelMeetingNeoEloquent\Models\Participant;

/**
 * Provides relationship methods for meeting model
 */
trait DefinesMeetingRelationship
{
    /**
     * Get the MorphTo Relation with the scheduler models
     * example: $scheduler = $meeting->scheduler, should be person who schedules it
     *
     * @return \Vinelab\NeoEloquent\Eloquent\Relations\MorphTo
     */
    public function scheduler(string $modelType): HasOne
    {
        return $this->hasOne($modelType, 'HAS_SCHEDULER');
        // return $this->hasOne(config('meeting.models.scheduler'));
        // return $this->morphTo();
    }

    /**
     * Get the MorphTo Relation with the host model
     *
     * @return \Vinelab\NeoEloquent\Eloquent\Relations\MorphTo
     */
    public function host(string $modelType): HasOne
    {
        return $this->hasOne($modelType, 'HAS_HOST');
        // return $this->morphTo();
    }

    /**
     * Get the MorphTo Relation with the presenter models
     *
     * @return \Vinelab\NeoEloquent\Eloquent\Relations\MorphTo
     */
    public function presenter(string $modelType): HasOne
    {
        return $this->hasOne($modelType, 'HAS_PRESENTER');
        // return $this->morphTo();
    }

    /**
     * Get the MorphToMany Relation with the participant models
     *
     * @param string $modelType
     * @return \Vinelab\NeoEloquent\Eloquent\Relations\MorphMany
     */
    public function participants(string $modelType): HasMany
    {
        return $this->hasMany($modelType, 'HAS_PARTICIPANT');
        // return $this->morphMany('Participant', 'IN');
        // return $this->morphedByMany($modelType, 'participant', 'meeting_participants')
        //             ->using(Participant::class)
        //             ->withPivot(['uuid', 'started_at', 'ended_at'])
        //             ->withTimestamps();
    }

    /**
     * Get the hasMany relationship with the participants pivot model
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function participantsPivot(): HasMany
    {
        return $this->hasMany(Participant::class);
    }
}
