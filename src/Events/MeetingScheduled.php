<?php

namespace FirstIgnite\LaravelMeetingNeoEloquent\Events;

use Illuminate\Queue\SerializesModels;
use FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting;

class MeetingScheduled
{
    use SerializesModels;

    public Meeting $meeting;

    /**
     * Create a new event instance.
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting $meeting
     */
    public function __construct(Meeting $meeting)
    {
        $this->meeting = $meeting;
    }
}
