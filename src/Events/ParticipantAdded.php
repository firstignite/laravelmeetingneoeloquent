<?php

namespace FirstIgnite\LaravelMeetingNeoEloquent\Events;

use Illuminate\Queue\SerializesModels;
// use FirstIgnite\LaravelMeetingNeoEloquent\Models\Participant;
use FirstIgnite\LaravelMeetingNeoEloquent\Contracts\Participant;

class ParticipantAdded
{
    use SerializesModels;

    public Participant $participant;

    /**
     * Create a new event instance.
     *
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Contracts\Participant $participant
     */
    public function __construct(Participant $participant)
    {
        $this->participant = $participant;
    }
}
