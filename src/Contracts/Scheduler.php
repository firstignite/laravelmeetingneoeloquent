<?php

namespace FirstIgnite\LaravelMeetingNeoEloquent\Contracts;

use Carbon\Carbon;
use Vinelab\NeoEloquent\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Vinelab\NeoEloquent\Eloquent\Relations\BelongsToMany;
use FirstIgnite\LaravelMeetingNeoEloquent\MeetingAdder;
use FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting;

interface Scheduler
{
    /**
     * Get the MorphMany Relation with the Meeting Model
     *
     * @return \Vinelab\NeoEloquent\Eloquent\Relations\BelongsToMany
     */
    public function meetings(): BelongsToMany;

    /**
    * Undocumented function
    *
    * @param string|null $provider
    * @return \FirstIgnite\LaravelMeetingNeoEloquent\MeetingAdder
    */
    public function scheduleMeeting(?string $provider = null): MeetingAdder;

    /**
     * Undocumented function
     *
     * @param \Vinelab\NeoEloquent\Eloquent\Builder $query
     * @param \Carbon\Carbon $start
     * @param \Carbon\Carbon $end
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting|null $except
     * @return \Vinelab\NeoEloquent\Eloquent\Builder
     */
    public function scopeAvailableBetween(Builder $query, Carbon $start, Carbon $end, ?Meeting $except = null): Builder;

    /**
     * Undocumented function
     *
     * @param \Vinelab\NeoEloquent\Eloquent\Builder $query
     * @param \Carbon\Carbon $start
     * @param \Carbon\Carbon $end
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting|null $except
     * @return \Vinelab\NeoEloquent\Eloquent\Builder
     */
    public function scopeBusyBetween(Builder $query, Carbon $start, Carbon $end, ?Meeting $except = null): Builder;

    /**
     * Undocumented function
    *
    * @param \Carbon\Carbon $start
    * @param \Carbon\Carbon $end
    * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting|null $except
    * @return bool
    */
    public function isAvailableBetween(Carbon $start, Carbon $end, ?Meeting $except = null): bool;

    /**
     * Undocumented function
     *
     * @param \Carbon\Carbon $start
     * @param \Carbon\Carbon $end
     * @param \FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting|null $except
     * @return bool
     */
    public function isBusyBetween(Carbon $start, Carbon $end, ?Meeting $except = null): bool;
}
