<?php

namespace FirstIgnite\LaravelMeetingNeoEloquent\Concerns;

// use Illuminate\Database\Eloquent\Relations\MorphMany;
use Vinelab\NeoEloquent\Eloquent\Relations\BelongsToMany;
use Vinelab\NeoEloquent\Eloquent\Relations\MorphMany;
use FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting;
use FirstIgnite\LaravelMeetingNeoEloquent\Models\Traits\VerifiesAvailability;

/**
 * Provides default implementation of Presenter contract.
 */
trait PresentsMeetings
{
    use VerifiesAvailability;
    
    /**
     * Get the MorphMany Relation with the Meeting Model
     *
     * @return \Vinelab\NeoEloquent\Eloquent\Relations\BelongsToMany
     */
    public function meetings(): BelongsToMany
    {
        // return $this->morphMany('Meeting', 'PRESENTED_BY');
        return $this->belongsToMany('Meeting', 'HAS_PRESENTER')->with('scheduler', 'host');
        // return $this->morphMany(Meeting::class, 'presenter')->with('scheduler', 'host');
    }
}
