<?php

namespace FirstIgnite\LaravelMeetingNeoEloquent\Concerns;

use Illuminate\Database\Eloquent\Relations\MorphMany;
use Vinelab\NeoEloquent\Eloquent\Relations\BelongsToMany;
use FirstIgnite\LaravelMeetingNeoEloquent\MeetingAdder;
use FirstIgnite\LaravelMeetingNeoEloquent\Models\Meeting;
use FirstIgnite\LaravelMeetingNeoEloquent\Models\Traits\VerifiesAvailability;

/**
 * Provides default implementation of Scheduler contract.
 */
trait SchedulesMeetings
{
    use VerifiesAvailability;
    
    /**
     * Get the MorphMany Relation with the Meeting Model
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function meetings(): BelongsToMany
    {
        return $this->belongsToMany('Meeting', 'HAS_SCHEDULER')->with('presenter', 'host');
        // return $this->morphMany(Meeting::class, 'scheduler')->with('presenter', 'host');
        // return $this->morphMany('Meeting', 'SCHEDULED_BY');
    }

    /**
     * Undocumented function
     *
     * @param string|null $provider
     * @return \FirstIgnite\LaravelMeetingNeoEloquent\MeetingAdder
     */
    public function scheduleMeeting(?string $provider = null): MeetingAdder
    {
        return app(MeetingAdder::class)->withProvider($provider)->scheduledBy($this);
    }
}
